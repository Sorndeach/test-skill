<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller 
{
    public function index()
    {
        $output['list_videos'] = [];

        // GET DATA JSON
        $result_array = get_json_api('https://s3-ap-southeast-1.amazonaws.com/ysetter/media/video-search.json');

        // FILTER DATA AND COVERT TO ARRAY
        foreach ($result_array->items as $value) {
            $image_thumb = '';
            if (!empty($value->snippet->thumbnails->high->url)) {
                $image_thumb = $value->snippet->thumbnails->high->url;
            } elseif(!empty($value->snippet->thumbnails->medium->url)) {
                $image_thumb = $value->snippet->thumbnails->medium->url;
            } else {
                $image_thumb = $value->snippet->thumbnails->default->url;
            }

            if(!empty($value->id->videoId)){
                $output['list_videos'][] = [
                    'video_id' => $value->id->videoId,
                    'image_thumb' => $image_thumb,
                    'title' => !empty($value->snippet->title) ? $value->snippet->title : ''
                ];
            }
        }
        $this->load->view('list_video', $output);
    }
}
