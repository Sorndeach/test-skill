<?php

    function pr($data) 
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    function get_json_api($url)
    {
        $result_array = [];
        $ch =  curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($ch);
        curl_close($ch);
        $result_array = json_decode($json);

        return $result_array;
    }

    function check_code_http($url='')
    {
        $http_code = '';
        $ch = curl_init($url);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $http_code;
    }