<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Programmer Skill Test</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-icon.css'); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?>?v=<?php echo time();?>">
    </head>
    <body>
        <div class="container mt-5">
            <div class="card">
                <div class="card-header">Video Search Result</div>
                <div class="card-body">
                    <div class="row">
                    <?php foreach ($list_videos as $video) : ?>
                        <div class="col-sm-4">
                            <div class="card">
                                <div class="video-container">
                                    <div class="btn-play" data-vid="<?php echo $video['video_id']?>">
                                        <i class="fas fa-play"></i>
                                    </div>
                                    <img class="card-img-top" src="<?php echo $video['image_thumb']?>" alt="<?php echo $video['title']?>">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">Title</h5>
                                    <p class="card-text"><?php echo $video['title']?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.slim.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.btn-play').on('click', function(){
                    var this_vid = $(this).data('vid');
                    var _parent_this = $(this).parent();
                    if (this_vid) {
                        $(this).addClass('hidden');
                        _parent_this.find('.card-img-top');                      
                        autoPlayVideo(_parent_this,this_vid);
                    } else {
                        alert('Video is empty ...');
                    }
                    
                });

                function autoPlayVideo(ele_target,vcode){
                  ele_target.html('<iframe width="100%" height="250px" src="https://www.youtube.com/embed/'+vcode+'?autoplay=1&loop=1&rel=0&wmode=transparent"frameborder="0"></iframe>');
                }
            });
        </script>
    </body>
</html>